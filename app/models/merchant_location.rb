# == Schema Information
#
# Table name: merchant_locations
#
#  id                      :integer          not null, primary key
#  name                    :string           not null
#  merchant_id             :integer
#  store_number            :string
#  street1                 :string
#  street2                 :string
#  city                    :string
#  state                   :string
#  postal_code             :string
#  primary_phone           :string
#  fax                     :string
#  primary_contact_user_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  bank_account_id         :integer
#

class MerchantLocation < ActiveRecord::Base
  belongs_to :merchant
  belongs_to :primary_contact_user, class_name: 'User'
  belongs_to :bank_account
  has_one :ach_customer, through: :bank_account

  has_many :sale_ach_transfers, through: :bank_account, source: :received_ach_transfers
  has_many :return_ach_transfers, through: :bank_account, source: :sent_ach_transfers

  has_many :merchant_terminals
  has_many :card_transactions, through: :merchant_terminals

  has_many :available_bank_accounts, through: :merchant, source: :bank_accounts

  validates_presence_of :merchant_id, :name
  validates_uniqueness_of :name, scope: :merchant_id

  validate :validate_bank_account, if: :bank_account_set?

  scope :ordered, -> { order(:name) }

  def last_week_return_total
    return_ach_transfers.for_past_week.sum(:amount)
  end

  def funding_instrument_id_token
    bank_account.funding_instrument_id_token if bank_account
  end

  def validate_bank_account
    errors.add(:bank_account_id, 'is invalid') unless available_bank_accounts.exists?(id: bank_account_id)
  end

  def bank_account_set?
    bank_account_id.present?
  end
end
