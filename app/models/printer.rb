# == Schema Information
#
# Table name: printers
#
#  id                   :integer          not null, primary key
#  merchant_location_id :integer
#  mfg                  :string
#  model                :string
#  interface            :string
#  driver               :string
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class Printer < ActiveRecord::Base
end
