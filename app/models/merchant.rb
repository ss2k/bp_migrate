# == Schema Information
#
# Table name: merchants
#
#  id                      :integer          not null, primary key
#  name                    :string           not null
#  merchant_id             :string
#  street1                 :string
#  street2                 :string
#  city                    :string
#  state                   :string
#  postal_code             :string
#  primary_phone           :string
#  fax                     :string
#  primary_contact_user_id :integer
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#  status                  :string
#  division                :string
#  website                 :string
#  note                    :text
#  franchise               :boolean
#  national                :boolean
#

class Merchant < ActiveRecord::Base
  belongs_to :primary_contact_user, class_name: 'User'
  has_many :merchant_locations
  has_one :ach_customer, as: :bankable
  has_many :bank_accounts, through: :ach_customer

  validates_presence_of :name

  scope :ordered, -> { order(:name) }
end
