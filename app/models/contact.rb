# == Schema Information
#
# Table name: contacts
#
#  id                   :integer          not null, primary key
#  merchant_location_id :integer
#  first_name           :string
#  last_name            :string
#  email                :string
#  note                 :text
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#

class Contact < ActiveRecord::Base
end
