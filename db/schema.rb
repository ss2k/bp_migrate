# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160903205828) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "citext"

  create_table "ach_customers", force: :cascade do |t|
    t.integer  "bankable_id",        null: false
    t.string   "bankable_type",      null: false
    t.string   "ach_customer_token", null: false
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "status"
    t.string   "email"
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "ach_agreed_at"
  end

  add_index "ach_customers", ["ach_customer_token"], name: "index_ach_customers_on_ach_customer_token", using: :btree
  add_index "ach_customers", ["bankable_type", "bankable_id"], name: "index_ach_customers_on_bankable_type_and_bankable_id", using: :btree
  add_index "ach_customers", ["email"], name: "index_ach_customers_on_email", using: :btree

  create_table "ach_events", force: :cascade do |t|
    t.string   "event_id",                      null: false
    t.string   "topic",                         null: false
    t.string   "resource_id"
    t.string   "event_link"
    t.string   "resource_link"
    t.string   "account_link"
    t.string   "customer_link"
    t.datetime "timestamp"
    t.boolean  "processed",     default: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "ach_events", ["event_id"], name: "index_ach_events_on_event_id", using: :btree
  add_index "ach_events", ["topic"], name: "index_ach_events_on_topic", using: :btree

  create_table "ach_oauth_tokens", force: :cascade do |t|
    t.string   "account_id"
    t.string   "access_token"
    t.string   "refresh_token"
    t.datetime "expires_at"
    t.datetime "refresh_expires_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  create_table "ach_transfers", force: :cascade do |t|
    t.string   "sender_funding_source_token",                               null: false
    t.string   "receiver_funding_source_token",                             null: false
    t.decimal  "amount",                           precision: 15, scale: 2
    t.string   "transfer_type",                                             null: false
    t.datetime "created_at",                                                null: false
    t.datetime "updated_at",                                                null: false
    t.integer  "transferable_id"
    t.string   "transferable_type"
    t.string   "transfer_token"
    t.string   "receiver_status"
    t.string   "sender_status"
    t.datetime "initiated_provider_email_sent_at"
    t.datetime "completed_provider_email_sent_at"
    t.datetime "initiated_merchant_email_sent_at"
    t.datetime "completed_merchant_email_sent_at"
    t.datetime "initiated_funder_email_sent_at"
    t.datetime "completed_funder_email_sent_at"
  end

  add_index "ach_transfers", ["receiver_funding_source_token"], name: "index_ach_transfers_on_receiver_funding_source_token", using: :btree
  add_index "ach_transfers", ["receiver_status"], name: "index_ach_transfers_on_receiver_status", using: :btree
  add_index "ach_transfers", ["sender_funding_source_token"], name: "index_ach_transfers_on_sender_funding_source_token", using: :btree
  add_index "ach_transfers", ["sender_status"], name: "index_ach_transfers_on_sender_status", using: :btree
  add_index "ach_transfers", ["transfer_type"], name: "index_ach_transfers_on_transfer_type", using: :btree
  add_index "ach_transfers", ["transferable_type", "transferable_id"], name: "index_ach_transfers_on_transferable_type_and_transferable_id", using: :btree

  create_table "auth_tokens", force: :cascade do |t|
    t.string   "token_lookup_hash", null: false
    t.text     "token",             null: false
    t.integer  "user_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "auth_tokens", ["token_lookup_hash"], name: "index_auth_tokens_on_token_lookup_hash", using: :btree
  add_index "auth_tokens", ["user_id"], name: "index_auth_tokens_on_user_id", using: :btree

  create_table "bank_accounts", force: :cascade do |t|
    t.string   "account_name",                            null: false
    t.string   "company_name"
    t.string   "account_type"
    t.string   "bank_phone_number"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "funding_instrument_id_token"
    t.integer  "ach_customer_id",                         null: false
    t.string   "status"
    t.datetime "dwolla_ach_agreed_at"
    t.integer  "verification_attempt_count",  default: 0
  end

  add_index "bank_accounts", ["ach_customer_id"], name: "index_bank_accounts_on_ach_customer_id", using: :btree
  add_index "bank_accounts", ["funding_instrument_id_token"], name: "index_bank_accounts_on_funding_instrument_id_token", using: :btree

  create_table "buildpay_ach_tokens", force: :cascade do |t|
    t.string   "account_token",        null: false
    t.string   "funding_source_token", null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "card_load_approvals", force: :cascade do |t|
    t.integer  "user_id",      null: false
    t.integer  "card_load_id", null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "card_load_approvals", ["card_load_id"], name: "index_card_load_approvals_on_card_load_id", using: :btree
  add_index "card_load_approvals", ["user_id"], name: "index_card_load_approvals_on_user_id", using: :btree

  create_table "card_loads", force: :cascade do |t|
    t.integer  "project_id",                                               null: false
    t.integer  "user_id",                                                  null: false
    t.decimal  "fund_amount",     precision: 15, scale: 2,                 null: false
    t.boolean  "approved",                                 default: false
    t.datetime "approved_at"
    t.boolean  "acknowledged",                             default: false
    t.datetime "acknowledged_at"
    t.datetime "email_sent_at"
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
  end

  add_index "card_loads", ["project_id"], name: "index_card_loads_on_project_id", using: :btree
  add_index "card_loads", ["user_id"], name: "index_card_loads_on_user_id", using: :btree

  create_table "card_transaction_line_items", force: :cascade do |t|
    t.integer  "card_transaction_id",                          null: false
    t.string   "sku_number"
    t.integer  "quantity"
    t.string   "how_sold"
    t.string   "description"
    t.integer  "total_quantity"
    t.decimal  "unit_price"
    t.string   "per"
    t.decimal  "net_amount",          precision: 15, scale: 2
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  add_index "card_transaction_line_items", ["card_transaction_id"], name: "index_on_crd_trans_id", using: :btree

  create_table "card_transactions", force: :cascade do |t|
    t.decimal  "amount",                  precision: 15, scale: 2,                null: false
    t.integer  "project_card_id",                                                 null: false
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.datetime "transacted_at",                                                   null: false
    t.string   "invoice_number"
    t.string   "reference_number"
    t.string   "sold_by"
    t.string   "slsp_number"
    t.string   "cshr_number"
    t.decimal  "subtotal",                precision: 15, scale: 2
    t.decimal  "sales_tax_percent"
    t.decimal  "sales_tax_amount",        precision: 15, scale: 2
    t.integer  "merchant_terminal_id"
    t.string   "terminal_transaction_id"
    t.boolean  "sale",                                             default: true
  end

  add_index "card_transactions", ["merchant_terminal_id"], name: "index_card_transactions_on_merchant_terminal_id", using: :btree
  add_index "card_transactions", ["project_card_id"], name: "index_card_transactions_on_project_card_id", using: :btree
  add_index "card_transactions", ["sale"], name: "index_card_transactions_on_sale", using: :btree

  create_table "committed_funds", force: :cascade do |t|
    t.integer  "project_id",                                               null: false
    t.integer  "from_user_id",                                             null: false
    t.integer  "to_user_id",                                               null: false
    t.datetime "created_at",                                               null: false
    t.datetime "updated_at",                                               null: false
    t.datetime "acknowledged_at"
    t.decimal  "fund_amount",     precision: 15, scale: 2,                 null: false
    t.string   "fund_type",                                                null: false
    t.boolean  "acknowledged",                             default: false
  end

  add_index "committed_funds", ["acknowledged"], name: "index_committed_funds_on_acknowledged", using: :btree
  add_index "committed_funds", ["from_user_id"], name: "index_committed_funds_on_from_user_id", using: :btree
  add_index "committed_funds", ["fund_type"], name: "index_committed_funds_on_fund_type", using: :btree
  add_index "committed_funds", ["project_id"], name: "index_committed_funds_on_project_id", using: :btree
  add_index "committed_funds", ["to_user_id"], name: "index_committed_funds_on_to_user_id", using: :btree

  create_table "contacts", force: :cascade do |t|
    t.integer  "merchant_location_id"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.text     "note"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "funder_organization_office_members", force: :cascade do |t|
    t.integer  "user_id",                                      null: false
    t.integer  "funder_organization_office_id",                null: false
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.integer  "parent_id"
    t.boolean  "can_create_projects",           default: true
  end

  add_index "funder_organization_office_members", ["funder_organization_office_id"], name: "index_funder_org_office_members_on_office_id", using: :btree
  add_index "funder_organization_office_members", ["parent_id"], name: "index_funder_organization_office_members_on_parent_id", using: :btree
  add_index "funder_organization_office_members", ["user_id", "funder_organization_office_id"], name: "index_user_id_funder_org_office_id", unique: true, using: :btree
  add_index "funder_organization_office_members", ["user_id"], name: "index_funder_organization_office_members_on_user_id", using: :btree

  create_table "funder_organization_offices", force: :cascade do |t|
    t.integer  "funder_organization_id"
    t.string   "name",                    null: false
    t.string   "dba"
    t.string   "ein"
    t.string   "street1"
    t.string   "street2"
    t.string   "city"
    t.string   "state"
    t.string   "postal_code"
    t.string   "website"
    t.string   "primary_phone"
    t.string   "fax"
    t.integer  "primary_contact_user_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "company_type"
  end

  add_index "funder_organization_offices", ["funder_organization_id"], name: "index_funder_organization_offices_on_funder_organization_id", using: :btree
  add_index "funder_organization_offices", ["name"], name: "index_funder_organization_offices_on_name", using: :btree
  add_index "funder_organization_offices", ["primary_contact_user_id"], name: "index_funder_organization_offices_on_primary_contact_user_id", using: :btree

  create_table "funder_organizations", force: :cascade do |t|
    t.string   "name",        null: false
    t.decimal  "service_fee"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "funder_organizations", ["name"], name: "index_funder_organizations_on_name", using: :btree

  create_table "invites", force: :cascade do |t|
    t.citext   "email",              null: false
    t.integer  "user_id"
    t.datetime "email_sent_at"
    t.string   "hash_id",            null: false
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "created_by_user_id"
  end

  add_index "invites", ["created_by_user_id"], name: "index_invites_on_created_by_user_id", using: :btree
  add_index "invites", ["email"], name: "index_invites_on_email", unique: true, using: :btree
  add_index "invites", ["hash_id"], name: "index_invites_on_hash_id", using: :btree
  add_index "invites", ["user_id"], name: "index_invites_on_user_id", using: :btree

  create_table "merchant_locations", force: :cascade do |t|
    t.string   "name",                    null: false
    t.integer  "merchant_id"
    t.string   "store_number"
    t.string   "street1"
    t.string   "street2"
    t.string   "city"
    t.string   "state"
    t.string   "postal_code"
    t.string   "primary_phone"
    t.string   "fax"
    t.integer  "primary_contact_user_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "bank_account_id"
    t.string   "county"
    t.boolean  "headquarter"
    t.boolean  "verified"
    t.boolean  "in_pilot"
    t.string   "note"
    t.string   "status"
  end

  add_index "merchant_locations", ["bank_account_id"], name: "index_merchant_locations_on_bank_account_id", using: :btree
  add_index "merchant_locations", ["merchant_id"], name: "index_merchant_locations_on_merchant_id", using: :btree
  add_index "merchant_locations", ["name"], name: "index_merchant_locations_on_name", using: :btree
  add_index "merchant_locations", ["primary_contact_user_id"], name: "index_merchant_locations_on_primary_contact_user_id", using: :btree

  create_table "merchant_terminals", force: :cascade do |t|
    t.string   "terminal_identifier",  null: false
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "merchant_location_id"
    t.string   "serial_number"
  end

  add_index "merchant_terminals", ["merchant_location_id"], name: "index_merchant_terminals_on_merchant_location_id", using: :btree
  add_index "merchant_terminals", ["serial_number"], name: "index_merchant_terminals_on_serial_number", using: :btree
  add_index "merchant_terminals", ["terminal_identifier"], name: "index_merchant_terminals_on_terminal_identifier", using: :btree

  create_table "merchants", force: :cascade do |t|
    t.string   "name",                    null: false
    t.string   "merchant_id"
    t.string   "street1"
    t.string   "street2"
    t.string   "city"
    t.string   "state"
    t.string   "postal_code"
    t.string   "primary_phone"
    t.string   "fax"
    t.integer  "primary_contact_user_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "status"
    t.string   "division"
    t.string   "website"
    t.text     "note"
    t.boolean  "franchise"
    t.boolean  "national"
  end

  add_index "merchants", ["name"], name: "index_merchants_on_name", using: :btree
  add_index "merchants", ["primary_contact_user_id"], name: "index_merchants_on_primary_contact_user_id", using: :btree

  create_table "payout_approvals", force: :cascade do |t|
    t.integer  "user_id",    null: false
    t.integer  "payout_id",  null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "payout_approvals", ["payout_id"], name: "index_payout_approvals_on_payout_id", using: :btree
  add_index "payout_approvals", ["user_id"], name: "index_payout_approvals_on_user_id", using: :btree

  create_table "payouts", force: :cascade do |t|
    t.integer  "project_id"
    t.integer  "user_id",                                                        null: false
    t.decimal  "fund_amount",           precision: 15, scale: 2,                 null: false
    t.datetime "created_at",                                                     null: false
    t.datetime "updated_at",                                                     null: false
    t.datetime "approved_at"
    t.boolean  "approved",                                       default: false
    t.boolean  "acknowledged",                                   default: false
    t.datetime "acknowledged_at"
    t.datetime "release_email_sent_at"
  end

  add_index "payouts", ["acknowledged"], name: "index_payouts_on_acknowledged", using: :btree
  add_index "payouts", ["approved"], name: "index_payouts_on_approved", using: :btree
  add_index "payouts", ["project_id"], name: "index_payouts_on_project_id", using: :btree
  add_index "payouts", ["user_id"], name: "index_payouts_on_user_id", using: :btree

  create_table "pos", force: :cascade do |t|
    t.integer  "merchant_location_id"
    t.string   "mfg"
    t.string   "model"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "printers", force: :cascade do |t|
    t.integer  "merchant_location_id"
    t.string   "mfg"
    t.string   "model"
    t.string   "interface"
    t.string   "driver"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "project_cards", force: :cascade do |t|
    t.integer  "project_service_provider_id"
    t.string   "card_number",                                                          null: false
    t.datetime "created_at",                                                           null: false
    t.datetime "updated_at",                                                           null: false
    t.decimal  "balance",                     precision: 15, scale: 2, default: 0.0
    t.boolean  "on_hold",                                              default: false
  end

  add_index "project_cards", ["card_number"], name: "index_project_cards_on_card_number", using: :btree
  add_index "project_cards", ["project_service_provider_id"], name: "index_on_psp_id", using: :btree

  create_table "project_member_notifications", force: :cascade do |t|
    t.integer  "project_id"
    t.integer  "user_id"
    t.string   "event_type"
    t.integer  "notifiable_id"
    t.string   "notifiable_type"
    t.boolean  "dismissed",       default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

  add_index "project_member_notifications", ["notifiable_type", "notifiable_id"], name: "index_project_member_notifications_on_notifiable_id_and_type", using: :btree
  add_index "project_member_notifications", ["project_id"], name: "index_project_member_notifications_on_project_id", using: :btree
  add_index "project_member_notifications", ["user_id"], name: "index_project_member_notifications_on_user_id", using: :btree

  create_table "project_service_providers", force: :cascade do |t|
    t.integer  "project_id",                                 null: false
    t.integer  "user_id",                                    null: false
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.integer  "parent_id"
    t.boolean  "card_requested",             default: false, null: false
    t.datetime "card_requested_at"
    t.datetime "card_request_email_sent_at"
    t.string   "role_description"
    t.integer  "bank_account_id"
  end

  add_index "project_service_providers", ["bank_account_id"], name: "index_project_service_providers_on_bank_account_id", using: :btree
  add_index "project_service_providers", ["project_id"], name: "index_project_service_providers_on_project_id", using: :btree
  add_index "project_service_providers", ["user_id"], name: "index_project_service_providers_on_user_id", using: :btree

  create_table "projects", force: :cascade do |t|
    t.string   "name"
    t.citext   "claim_number",                                                                null: false
    t.text     "description"
    t.string   "address_1"
    t.string   "address_2"
    t.string   "city"
    t.string   "state"
    t.string   "postal_code"
    t.text     "comments"
    t.decimal  "material_funds",                       precision: 15, scale: 2, default: 0.0, null: false
    t.datetime "created_at",                                                                  null: false
    t.datetime "updated_at",                                                                  null: false
    t.integer  "created_by_user_id"
    t.integer  "owner_user_id"
    t.decimal  "labor_funds",                          precision: 15, scale: 2, default: 0.0, null: false
    t.decimal  "overhead_funds",                       precision: 15, scale: 2, default: 0.0, null: false
    t.decimal  "profit_funds",                         precision: 15, scale: 2, default: 0.0, null: false
    t.datetime "started_at"
    t.integer  "bank_account_id"
    t.integer  "funder_organization_office_id"
    t.integer  "funder_organization_office_member_id"
    t.integer  "funder_organization_id"
  end

  add_index "projects", ["bank_account_id"], name: "index_projects_on_bank_account_id", using: :btree
  add_index "projects", ["created_by_user_id"], name: "index_projects_on_created_by_user_id", using: :btree
  add_index "projects", ["funder_organization_id"], name: "index_projects_on_funder_organization_id", using: :btree
  add_index "projects", ["funder_organization_office_id"], name: "index_projects_on_funder_organization_office_id", using: :btree
  add_index "projects", ["funder_organization_office_member_id"], name: "index_projects_on_funder_organization_office_member_id", using: :btree
  add_index "projects", ["owner_user_id"], name: "index_projects_on_owner_user_id", using: :btree

  create_table "service_fees", force: :cascade do |t|
    t.decimal  "amount",       precision: 15, scale: 2, null: false
    t.integer  "feeable_id",                            null: false
    t.string   "feeable_type",                          null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "service_fees", ["feeable_type", "feeable_id"], name: "index_service_fees_on_feeable_type_and_feeable_id", using: :btree

  create_table "system_roles", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.citext   "email",                        null: false
    t.string   "password_hash"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "status"
    t.datetime "registered_at"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.integer  "system_role_id"
    t.string   "home_street1"
    t.string   "home_street2"
    t.string   "home_city"
    t.string   "home_state"
    t.string   "home_postal_code"
    t.string   "home_phone"
    t.string   "home_mobile_phone"
    t.string   "company_name"
    t.string   "company_street1"
    t.string   "company_street2"
    t.string   "company_city"
    t.string   "company_state"
    t.string   "company_postal_code"
    t.string   "company_work_phone"
    t.string   "company_mobile_phone"
    t.string   "company_fax_phone"
    t.string   "company_url"
    t.string   "password_reset_hash"
    t.datetime "password_reset_at"
    t.datetime "password_reset_email_sent_at"
    t.datetime "tos_agreed_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["first_name"], name: "index_users_on_first_name", using: :btree
  add_index "users", ["last_name"], name: "index_users_on_last_name", using: :btree
  add_index "users", ["status"], name: "index_users_on_status", using: :btree
  add_index "users", ["system_role_id"], name: "index_users_on_system_role_id", using: :btree

  add_foreign_key "auth_tokens", "users"
  add_foreign_key "card_load_approvals", "card_loads"
  add_foreign_key "card_load_approvals", "users"
  add_foreign_key "card_loads", "projects"
  add_foreign_key "card_loads", "users"
  add_foreign_key "card_transaction_line_items", "card_transactions"
  add_foreign_key "card_transactions", "merchant_terminals"
  add_foreign_key "card_transactions", "project_cards"
  add_foreign_key "committed_funds", "projects"
  add_foreign_key "committed_funds", "users", column: "from_user_id"
  add_foreign_key "committed_funds", "users", column: "to_user_id"
  add_foreign_key "funder_organization_office_members", "funder_organization_offices"
  add_foreign_key "funder_organization_office_members", "users"
  add_foreign_key "invites", "users"
  add_foreign_key "invites", "users", column: "created_by_user_id"
  add_foreign_key "merchant_locations", "bank_accounts"
  add_foreign_key "merchant_locations", "merchants"
  add_foreign_key "merchant_locations", "users", column: "primary_contact_user_id"
  add_foreign_key "merchant_terminals", "merchant_locations"
  add_foreign_key "merchants", "users", column: "primary_contact_user_id"
  add_foreign_key "payout_approvals", "payouts"
  add_foreign_key "payout_approvals", "users"
  add_foreign_key "payouts", "projects"
  add_foreign_key "payouts", "users"
  add_foreign_key "project_cards", "project_service_providers"
  add_foreign_key "project_member_notifications", "projects"
  add_foreign_key "project_member_notifications", "users"
  add_foreign_key "project_service_providers", "bank_accounts"
  add_foreign_key "project_service_providers", "projects"
  add_foreign_key "project_service_providers", "users"
  add_foreign_key "projects", "bank_accounts"
  add_foreign_key "projects", "funder_organization_office_members"
  add_foreign_key "projects", "funder_organization_offices"
  add_foreign_key "projects", "funder_organizations"
  add_foreign_key "projects", "users", column: "created_by_user_id"
  add_foreign_key "projects", "users", column: "owner_user_id"
  add_foreign_key "users", "system_roles"
end
