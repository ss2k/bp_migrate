class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.integer :merchant_location_id
      t.string :first_name
      t.string :last_name
      t.string :email
      t.text :note

      t.timestamps null: false
    end
  end
end
