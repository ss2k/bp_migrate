class AddNewFieldsToMerchantLocations < ActiveRecord::Migration
  def change
    add_column :merchant_locations, :county, :string
    add_column :merchant_locations, :headquarter, :boolean
    add_column :merchant_locations, :verified, :boolean
    add_column :merchant_locations, :in_pilot, :boolean
    add_column :merchant_locations, :note, :string
    add_column :merchant_locations, :status, :string
  end
end
