class AddNewFieldsToMerchants < ActiveRecord::Migration
  def change
    add_column :merchants, :status, :string
    add_column :merchants, :division, :string
    add_column :merchants, :website, :string
    add_column :merchants, :note, :text
    add_column :merchants, :franchise, :boolean
    add_column :merchants, :national, :boolean
  end
end
