class CreatePos < ActiveRecord::Migration
  def change
    create_table :pos do |t|
      t.integer :merchant_location_id
      t.string :mfg
      t.string :model

      t.timestamps null: false
    end
  end
end
