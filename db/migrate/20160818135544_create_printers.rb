class CreatePrinters < ActiveRecord::Migration
  def change
    create_table :printers do |t|
      t.integer :merchant_location_id
      t.string :mfg
      t.string :model
      t.string :interface
      t.string :driver

      t.timestamps null: false
    end
  end
end
