namespace :import_from_mysql do

  BASE_URL = "http://localhost:3000"

  desc "Import data from mysql"
  task :import_data => :environment do
    response = HTTParty.get(BASE_URL + "/exports")
    data_json = JSON.parse(response.body)
    process_data(data_json)
  end

  def process_data(json)
    json.each do |merchant|
      build_merchant(merchant)
    end
  end

  def build_merchant(merchant)
    new_merchant = Merchant.new(
      name: merchant["Merchant"],
      status: merchant["Status"],
      division: merchant["Division"],
      website: merchant["WebSite"],
      note: merchant["Note"],
      franchise: convert_to_boolean(merchant["Franchise"]),
      national: convert_to_boolean(merchant["Natonal"])
    )

    locations = merchant["addresses"]

    if new_merchant.save
      build_locations(new_merchant, locations) unless locations.empty?
    else
      puts "Error saving merchant: #{new_merchant.name} --- #{new_merchant.errors.messages}"
    end
  end

  def build_locations(merchant, locations)
    locations.each do |location|
      new_location = merchant.merchant_locations.new(
        name: "#{merchant.name} - #{location["Address"]}",
        store_number: location["StoreID"],
        street1: location["Address"],
        street2: location["Address2"],
        city: location["City"],
        state: location["State"],
        postal_code: location["Zip"],
        primary_phone: location["Phone"],
        county: location["County"],
        headquarter: convert_to_boolean(location["HQ"]),
        verified: convert_to_boolean(location["verified"]),
        in_pilot: convert_to_boolean(location["InPilot"]),
        note: location["Note"],
        status: location["Status"]
      )
      
      unless new_location.save
        puts "Error saving location #{new_location.name} --- #{new_location.errors.messages}"
      end
    end
  end

  def convert_to_boolean(field)
    begin
      !field.zero?
    rescue
      nil
    end
  end
end
